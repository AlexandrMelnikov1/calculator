package com.calculator.app.model

enum class Operation {
    NONE,
    PLUS,
    MINUS,
    DIV,
    MULT
}
