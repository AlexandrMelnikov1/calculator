package com.calculator.app.utils

import android.content.Context
import android.content.SharedPreferences

object SpHelper {

    private const val prefKey = "PREFS"

    private fun sharedPref(ctx: Context): SharedPreferences = ctx.getSharedPreferences(prefKey, 0)

    private fun editor(ctx: Context): SharedPreferences.Editor = sharedPref(ctx).edit()


    fun isAuth(ctx: Context): Boolean  = sharedPref(ctx).getBoolean("auth", false)

    fun setAuth(ctx: Context, state: Boolean) = editor(ctx).putBoolean("auth", state).apply()

    fun getPass(ctx: Context) = sharedPref(ctx).getString("pass", "741963")!!

    fun setPass(ctx: Context, pin: String) = editor(ctx).putString("pass", pin).apply()
}
