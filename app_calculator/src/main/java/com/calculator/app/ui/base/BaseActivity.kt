package com.calculator.app.ui.base

import android.content.Intent
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.calculator.app.ui.LoginActivity
import com.calculator.app.ui.MainActivity
import com.calculator.app.ui.PinActivity
import com.calculator.app.ui.base.NavConstants.ACTIVITY_LOGIN
import com.calculator.app.ui.base.NavConstants.ACTIVITY_MAIN
import com.calculator.app.ui.base.NavConstants.ACTIVITY_PIN_CODE

abstract class BaseActivity : AppCompatActivity() {

    var isNotCalcActivity = true

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                if (isNotCalcActivity) {
                    super.onBackPressed()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun navigateTo(name: String){
        when (name) {
            ACTIVITY_PIN_CODE -> startActivity(Intent(this, PinActivity::class.java))
            ACTIVITY_MAIN -> {
                val i = Intent(this, MainActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(i)
            }
            ACTIVITY_LOGIN -> {
                val i = Intent(this, LoginActivity::class.java)
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(i)
            }
        }
    }

}
