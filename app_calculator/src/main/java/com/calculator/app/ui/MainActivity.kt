package com.calculator.app.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.calculator.app.R
import com.calculator.app.ui.base.BaseActivity
import com.calculator.app.ui.base.NavConstants.ACTIVITY_CALC
import com.calculator.app.ui.base.NavConstants.ACTIVITY_LOGIN
import com.calculator.app.ui.base.NavConstants.ACTIVITY_PIN_CODE
import com.calculator.app.ui.base.NavConstants.ACTIVITY_SETTINGS
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.DividerDrawerItem
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), Drawer.OnDrawerItemClickListener, AHBottomNavigation.OnTabSelectedListener {

    private lateinit var drawer: Drawer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        init(savedInstanceState)
    }

    private fun init(bundle: Bundle?) {

        bottom_navigation.accentColor = ContextCompat.getColor(this, R.color.white)
        bottom_navigation.inactiveColor = ContextCompat.getColor(this, R.color.accent)
        bottom_navigation.defaultBackgroundColor = ContextCompat.getColor(this, R.color.colorPrimary)
        bottom_navigation.setTitleTypeface(ResourcesCompat.getFont(this@MainActivity, R.font.oswald_regular))
        bottom_navigation.titleState = AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE
        bottom_navigation.addItem(AHBottomNavigationItem(getString(R.string.bottom_item1), R.drawable.ic_bottom_item1))
        bottom_navigation.addItem(AHBottomNavigationItem(getString(R.string.bottom_item2), R.drawable.ic_bottom_item2))
        bottom_navigation.addItem(AHBottomNavigationItem(getString(R.string.bottom_item3), R.drawable.ic_bottom_item3))
        bottom_navigation.setOnTabSelectedListener(this)
        onTabSelected(0, true)

        drawer = DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar!!)
                .withRootView(R.id.drawer_fl)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .withSavedInstance(bundle)
                .addDrawerItems(
                        PrimaryDrawerItem().withName(getString(R.string.menu_item1)).withIdentifier(1)
                                .withIcon(R.drawable.ic_menu_item1)
                                .withSelectable(false)
                                .withTypeface(ResourcesCompat.getFont(this@MainActivity, R.font.oswald_medium)),

                        PrimaryDrawerItem().withName(getString(R.string.menu_item2)).withIdentifier(2)
                                .withIcon(R.drawable.ic_menu_item2)
                                .withSelectable(false)
                                .withTypeface(ResourcesCompat.getFont(this@MainActivity, R.font.oswald_medium)),
                        DividerDrawerItem(),
                        PrimaryDrawerItem().withName(getString(R.string.menu_item3)).withIdentifier(90)
                                .withIcon(R.drawable.ic_menu_item3)
                                .withSelectable(false)
                                .withTypeface(ResourcesCompat.getFont(this@MainActivity, R.font.oswald_regular)),

                        PrimaryDrawerItem().withName(getString(R.string.menu_item4)).withIdentifier(98)
                                .withIcon(R.drawable.ic_menu_item4)
                                .withSelectable(false)
                                .withTypeface(ResourcesCompat.getFont(this@MainActivity, R.font.oswald_regular)),

                        PrimaryDrawerItem().withName(getString(R.string.menu_item5)).withIdentifier(99)
                                .withIcon(R.drawable.ic_menu_item5)
                                .withSelectable(false)
                                .withTypeface(ResourcesCompat.getFont(this@MainActivity, R.font.oswald_regular))
                )
                .withSelectedItem(-1)
                .withOnDrawerItemClickListener(this)
                .build()
    }

    override fun onItemClick(view: View, position: Int, drawerItem: IDrawerItem<*, *>?): Boolean {
        if (drawerItem != null) {
            when (drawerItem.identifier.toInt()) {
                98 -> {
                    navigateTo(ACTIVITY_CALC)
                    return false
                }
                90 -> {
                    navigateTo(ACTIVITY_SETTINGS)
                    return false
                }
                2 -> {
                    navigateTo(ACTIVITY_PIN_CODE)
                    return false
                }
                1 -> {
                    AlertDialog.Builder(this)
                            .setTitle(getString(R.string.dialog_activity_login_title))
                            .setMessage(getString(R.string.dialog_activity_login_msg))
                            .setNegativeButton(getString(R.string.dialog_activity_login_negative), null)
                            .setPositiveButton(getString(R.string.dialog_activity_login_positive)) { _, _ ->
                                navigateTo(ACTIVITY_LOGIN)
                            }.show()
                    return false
                }
                else -> {
                    onBackPressed()
                    return false
                }
            }
        }
        return false
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    override fun setTitle(title: CharSequence) {
        title_toolbar!!.text = title
    }

    override fun onTabSelected(position: Int, wasSelected: Boolean): Boolean {
        val fragment = TaskFragment()
        title = when (position) {
            1 -> getString(R.string.bottom_item2)
            2 -> getString(R.string.bottom_item3)
            else -> getString(R.string.bottom_item1)
        }
        btn_add.visibility = if (position == 0) View.VISIBLE else View.GONE
        supportFragmentManager.beginTransaction().replace(R.id.fl, fragment).commit()
        return true
    }
}
