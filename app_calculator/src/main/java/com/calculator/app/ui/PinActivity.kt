package com.calculator.app.ui

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import com.calculator.app.R
import com.calculator.app.ui.base.BaseActivity
import com.calculator.app.ui.base.NavConstants
import com.calculator.app.utils.SpHelper
import kotlinx.android.synthetic.main.activity_pin.*

class PinActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pin)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_close)


        btn_install_pass.setOnClickListener {
            val pin: String = et_pin.text.toString()
            if (checkPinCode(pin)){
                navigateTo(NavConstants.ACTIVITY_MAIN)
            }
        }
    }


    private fun checkPinCode(pinCode: String): Boolean{
        return if (pinCode.length in 6..9 && pinCode.first() != '0'){
            SpHelper.setPass(this, pinCode)
            Toast.makeText(this, "Новый пин-код принят :)", Toast.LENGTH_SHORT).show()
            true
        }else{
            Toast.makeText(this, "Неверный пин-код!", Toast.LENGTH_SHORT).show()
            false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
