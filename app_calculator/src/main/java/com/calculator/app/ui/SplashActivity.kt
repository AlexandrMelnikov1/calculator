package com.calculator.app.ui

import android.content.Intent
import android.os.Bundle
import com.calculator.app.ui.base.BaseActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        openCalc()
    }

    private fun openCalc() {
        startActivity(Intent(this, CalcActivity::class.java))
        this.finish()
    }

    private fun openMain() {
        startActivity(Intent(this, MainActivity::class.java))
        this.finish()
    }
}
