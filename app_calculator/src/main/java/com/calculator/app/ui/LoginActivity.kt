package com.calculator.app.ui

import android.os.Bundle
import android.widget.Toast
import com.calculator.app.R
import com.calculator.app.ui.base.BaseActivity
import com.calculator.app.ui.base.NavConstants.ACTIVITY_MAIN
import com.calculator.app.utils.SpHelper
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close)

        btn_inter.setOnClickListener {
            if (et_login.text.toString() == "test" && et_pass.text.toString() == "123321"){
                SpHelper.setAuth(this, true)
                navigateTo(ACTIVITY_MAIN)
            }else{
                Toast.makeText(this, "error!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
