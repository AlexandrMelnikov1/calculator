package com.calculator.app.ui.base

object NavConstants{
    const val ACTIVITY_CALC = "ACTIVITY_CALC"
    const val ACTIVITY_PIN_CODE = "ACTIVITY_PIN_CODE"
    const val ACTIVITY_MAIN = "ACTIVITY_MAIN"
    const val ACTIVITY_LOGIN = "ACTIVITY_LOGIN"
    const val ACTIVITY_SETTINGS = "ACTIVITY_SETTINGS"

}