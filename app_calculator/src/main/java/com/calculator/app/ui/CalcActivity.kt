package com.calculator.app.ui

import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import com.calculator.app.R
import com.calculator.app.model.Operation
import com.calculator.app.ui.base.BaseActivity
import com.calculator.app.ui.base.NavConstants.ACTIVITY_LOGIN
import com.calculator.app.ui.base.NavConstants.ACTIVITY_PIN_CODE
import com.calculator.app.utils.SpHelper
import kotlinx.android.synthetic.main.activity_calc.*

class CalcActivity : BaseActivity() {

    private var num1 = ""
    private var num2: String? = ""
    private var operation = Operation.NONE

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isNotCalcActivity = false
        setContentView(R.layout.activity_calc)

        input.text = null
        initListeners()
    }

    private fun initListeners() {
        btn0.setOnClickListener {
            onClickNum((it as TextView).text)
        }
        btn1.setOnClickListener {
            onClickNum((it as TextView).text)
        }
        btn2.setOnClickListener {
            onClickNum((it as TextView).text)
        }
        btn3.setOnClickListener {
            onClickNum((it as TextView).text)
        }
        btn4.setOnClickListener {
            onClickNum((it as TextView).text)
        }
        btn5.setOnClickListener {
            onClickNum((it as TextView).text)
        }
        btn6.setOnClickListener {
            onClickNum((it as TextView).text)
        }
        btn7.setOnClickListener {
            onClickNum((it as TextView).text)
        }
        btn8.setOnClickListener {
            onClickNum((it as TextView).text)
        }
        btn9.setOnClickListener {
            onClickNum((it as TextView).text)
        }

        btnCE.setOnClickListener {
            onClickClear()
        }
        btnEqual.setOnClickListener {
            onClickResult()
        }
        btnPlus.setOnClickListener {
            if (num1.isNotEmpty()) onClickPlus()
        }
        btnMinus.setOnClickListener {
            if (num1.isNotEmpty()) onClickMinus()
        }
        btnDiv.setOnClickListener {
            if (num1.isNotEmpty()) onClickDiv()
        }
        btnMult.setOnClickListener {
            if (num1.isNotEmpty()) onClickMult()
        }
    }

    private fun onClickNum(symbol: CharSequence) {
        try {
            input!!.append(symbol)
            if (operation === Operation.NONE)
                num1 += symbol
            else
                num2 += symbol
        } catch (e: Exception) {
            Log.e("DEV", e.localizedMessage)
            Toast.makeText(this, "Error..", Toast.LENGTH_SHORT).show()
        }

    }

    private fun onClickPlus() {
        if (operation !== Operation.NONE && num2 != "")
            onClickResult()
        else if (operation !== Operation.NONE) input!!.text = num1
        operation = Operation.PLUS

        input!!.append("+")


    }

    private fun onClickMinus() {
        if (operation !== Operation.NONE && num2!!.isNotEmpty())
            onClickResult()
        else if (operation !== Operation.NONE) input!!.text = num1
        operation = Operation.MINUS

        input!!.append("-")

    }

    private fun onClickDiv() {
        if (operation !== Operation.NONE && num2!!.isNotEmpty())
            onClickResult()
        else if (operation !== Operation.NONE) input!!.text = num1
        operation = Operation.DIV

        input!!.append("/")

    }

    private fun onClickMult() {
        if (operation !== Operation.NONE && num2!!.isNotEmpty())
            onClickResult()
        else if (operation !== Operation.NONE) input!!.text = num1
        operation = Operation.MULT

        input!!.append("*")
    }

    private fun onClickClear() {
        operation = Operation.NONE
        input!!.text = null
        num1 = ""
        num2 = ""
    }

    private fun onClickResult() {
        try {
            Log.i("TAG", "pass in pref = " + SpHelper.getPass(this))
            Log.i("TAG", "number in display = $num1")
            if (SpHelper.getPass(this).equals(num1, true)) {
                nextLevel()
                return
            } else if (operation === Operation.NONE || num2 == null) return

            val n1 = java.lang.Long.parseLong(num1)
            var n2 = java.lang.Long.parseLong(num2!!)


            when (operation) {
                Operation.MINUS -> {
                    onClickClear()
                    onClickNum((n1 - n2).toString())
                }
                Operation.DIV -> {
                    onClickClear()
                    if (n2 == 0L) n2 = 1
                    onClickNum((n1 / n2).toString())
                }
                Operation.MULT -> {
                    onClickClear()
                    onClickNum((n1 * n2).toString())
                }
                else -> {
                    onClickClear()
                    onClickNum((n1 + n2).toString())
                }
            }
        } catch (e: Exception) {
            Log.e("DEV", e.localizedMessage)
            Toast.makeText(this, "Error..", Toast.LENGTH_SHORT).show()
        }

    }

    private fun nextLevel() {
        Toast.makeText(this, "next", Toast.LENGTH_SHORT).show()
        if (SpHelper.isAuth(this)) {
            navigateTo(ACTIVITY_PIN_CODE)
        }else{
            navigateTo(ACTIVITY_LOGIN)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }
}
